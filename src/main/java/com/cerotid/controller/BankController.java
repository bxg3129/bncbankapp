package com.cerotid.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.cerotid.bank.bo.BankBO;
import com.cerotid.bank.model.Customer;

@Controller
public class BankController {
	@Autowired
	private BankBO bankBO;
	
	BankController(){
		System.out.println("Initializing BankContoller");
	}
	
	@RequestMapping(value= {"/","/welcome"}, method=RequestMethod.GET)
	public ModelAndView welcome() {
		ModelAndView modelAndView = new ModelAndView();
		modelAndView.addObject("customers",bankBO.getBankDetails().getCustomers());
		modelAndView.setViewName("login");
		
		return modelAndView;		
	}
	
	@RequestMapping(value={"/register-customer"}, method= RequestMethod.GET)
	public ModelAndView registerCustomer() {
		Customer customer = new Customer();
		
		ModelAndView modelAndView = new ModelAndView();
		modelAndView.addObject("customer", customer);
		modelAndView.setViewName("register-customer");
		
		return modelAndView;
	}
	
	@RequestMapping(value={"/addCustomer"}, method= RequestMethod.POST)
	public ModelAndView addCustomer(@ModelAttribute("customer") Customer customer) {
		bankBO.addCustomer(customer);
		
		ModelAndView modelAndView = new ModelAndView();
		modelAndView.addObject("customers",bankBO.getBankDetails().getCustomers());
				
		modelAndView.setViewName("login");
		return modelAndView;
	}
	
	@RequestMapping(value={"/openAccountForm"}, method= RequestMethod.GET)
	public ModelAndView openAccountForm() {
		
		Customer customer=new Customer();
		ModelAndView modelAndView = new ModelAndView();
	
		modelAndView.addObject("customer", customer);
		modelAndView.setViewName("openAccountForm");
		
		return modelAndView;
		
		
	}
	
	@RequestMapping(value={"/openAccount"}, method= RequestMethod.POST)
	public ModelAndView openAccount(@ModelAttribute("customer") Customer customer) {
	
		ModelAndView modelAndView = new ModelAndView();
		String ssn=customer.getSsn();
		
		modelAndView.setViewName("AccountProfile");
		return modelAndView;
	}
}
