package com.cerotid;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BncBankAppApplication {

	public static void main(String[] args) {
		SpringApplication.run(BncBankAppApplication.class, args);
	}
}
