package com.cerotid.bank.bo;

import org.springframework.stereotype.Component;

import com.cerotid.bank.model.Account;
import com.cerotid.bank.model.Bank;
import com.cerotid.bank.model.Customer;
import com.cerotid.bank.model.Transaction;

@Component
public class BankBOImpl implements BankBO{
	private static Bank bank;
	
	static {
		bank = new Bank();
	}

	@Override
	public void addCustomer(Customer customer) {
		bank.getCustomers().add(customer);
		
		//TODO update database code goes here
	}

	@Override
	public boolean openAccount(Customer customer, Account account) {
		// TODO Auto-generated method stub
		if(bank.getCustomers().contains(customer)) {//Check if the customer exist in the Bank!
			int i =  bank.getCustomers().indexOf(customer);
			
			bank.getCustomers().get(i).addAccount(account);
			
			return true;
		}
		else {
			System.out.println("Customer do not exist!");
			
			return false;
		}
	}

	@Override
	public void sendMoney(Customer customer, Transaction transaction) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void depositMoneyInCustomerAccount(Customer customer) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void editCustomerInfo(Customer customer) {
		if(bank.getCustomers().contains(customer)) {
			//int index = bank.getCustomers().indexOf(customer);
			
			//Customer customerTobeEdited = bank.getCustomers().get(index);
			
			Customer customerTobeEdited = getCustomerInfo(customer.getSsn());
			
			customerTobeEdited.setFirstName(customer.getFirstName());
			customerTobeEdited.setLastName(customer.getLastName());
			customerTobeEdited.setAddress(customer.getAddress());
		}
	}
	
	public void printBankStatus() {
		System.out.println(bank);
	}
	
	

	public static Bank getBank() {
		return bank;
	}

	@Override
	public Customer getCustomerInfo(String ssn) {
		return bank.getCustomer(ssn);
	}

	@Override
	public Bank getBankDetails() {
		// TODO Auto-generated method stub
		return this.bank;
	}
	
	

}
