package com.cerotid.bo;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.fail;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.cerotid.bank.bo.BankBO;
import com.cerotid.bank.model.Customer;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest
@EnableConfigurationProperties
public class BankBOImplTest {
	@Autowired
	private BankBO bankBo;

	@Before
	public void setUp() throws Exception {
		bankBo.getBankDetails();
	}
	
	private void addCustomers() {
		Customer customer1 = new Customer();
		customer1.setFirstName("Bipin");
		customer1.setLastName("Ghimire");
		customer1.setAddress("123 Blah Blah St, NYC 11100");
		customer1.setSsn("222222222");
		
		bankBo.addCustomer(customer1);
		
		Customer customer2 = new Customer();
		customer2.setFirstName("David");
		customer2.setLastName("Sanchez");
		customer2.setAddress("100 State St, Euless TX 76040");
		customer2.setSsn("111111111");
		
		bankBo.addCustomer(customer2);
	}


	@Test
	public void testAddCustomer() {
		addCustomers();
		
		assertEquals("David", bankBo.getCustomerInfo("111111111").getFirstName());
		assertEquals("Bipin", bankBo.getCustomerInfo("222222222").getFirstName());
		assertNotEquals("David", bankBo.getCustomerInfo("222222222").getFirstName());
	}


	@Test
	public void testOpenAccount() {
		fail("Not yet implemented");
	}

	@Test
	public void testSendMoney() {
		fail("Not yet implemented");
	}

	@Test
	public void testDepositMoneyInCustomerAccount() {
		fail("Not yet implemented");
	}

	@Test
	public void testEditCustomerInfo() {
		addCustomers();
		
		Customer customer = getCustomerTobeEditedInfo();
		
		bankBo.editCustomerInfo(customer);
		assertEquals("1221 State Hwy. Grand Prairie TX 75019", bankBo.getCustomerInfo("111111111").getAddress());
	}

	private Customer getCustomerTobeEditedInfo() {
		Customer customer = new Customer();
		customer.setFirstName("David");
		customer.setLastName("Sanchez");
		customer.setAddress("1221 State Hwy. Grand Prairie TX 75019");
		customer.setSsn("111111111");
		
		return customer;
	}

	@Test
	public void testPrintBankStatus() {
		fail("Not yet implemented");
	}

	@Test
	public void testGetCustomerInfo() {
		fail("Not yet implemented");
	}

}
